﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {
	[HideInInspector] public GameManager gameManager;
	[HideInInspector] public AudioSource audioSource;
	public List<AudioClip> clips;

	void Start () {
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		audioSource = this.gameObject.GetComponent<AudioSource>();
	}

	public void PlayClip (int idx) {
		audioSource.clip = clips[idx];
		audioSource.Play();
	}
}
