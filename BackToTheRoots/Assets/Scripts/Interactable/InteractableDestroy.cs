﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableDestroy : Interactable {
	public InteractableDestroy(GameManager gm) : base(gm) { }

	public override void ExecuteEnter() {
		Object.Destroy(this.gameObject);
	}

	public override void ExecuteExit() {}
	public override void ExecuteToggle() {}
}
