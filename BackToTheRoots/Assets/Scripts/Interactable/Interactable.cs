﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Interactable : MonoBehaviour {
	protected GameManager gameManager;
	public List<string> infos;
	
	public Interactable (GameManager gm) {
		gameManager = gm;
	}
    
	abstract public void ExecuteEnter();
	abstract public void ExecuteExit();
	abstract public void ExecuteToggle();
}
