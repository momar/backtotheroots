﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableActivate : Interactable {
	public bool standardState;
    public InteractableActivate(GameManager gm) : base(gm) { }

	public override void ExecuteEnter() {
		foreach (var info in infos) {
			if (info == "MovingPlattform") {
				this.gameObject.GetComponent<MovingPlattform>().enabled = !standardState;
			}
			else if (info == "SpriteRenderer") {
				this.gameObject.GetComponent<SpriteRenderer>().enabled = !standardState;
			}
			else if (info == "BoxCollider2D") {
				this.gameObject.GetComponent<BoxCollider2D>().enabled = !standardState;
			}
		}
	}

	public override void ExecuteExit() {
		foreach (var info in infos) {
			if (info == "MovingPlattform") {
				MovingPlattform mp = this.gameObject.GetComponent<MovingPlattform>();
				mp.moveVec = Vector3.zero;
				mp.enabled = standardState;
			}
			else if (info == "SpriteRenderer") {
				this.gameObject.GetComponent<SpriteRenderer>().enabled = standardState;
			}
			else if (info == "BoxCollider2D") {
				this.gameObject.GetComponent<BoxCollider2D>().enabled = standardState;
			}
		}
	}

	public override void ExecuteToggle() {
		foreach (var info in infos) {
			if (info == "MovingPlattform") {
				this.gameObject.GetComponent<MovingPlattform>().enabled = !this.gameObject.GetComponent<MovingPlattform>().enabled;
			}
			else if (info == "SpriteRenderer") {
				this.gameObject.GetComponent<SpriteRenderer>().enabled = !this.gameObject.GetComponent<SpriteRenderer>().enabled;
			}
			else if (info == "BoxCollider2D") {
				this.gameObject.GetComponent<BoxCollider2D>().enabled = !this.gameObject.GetComponent<BoxCollider2D>().enabled;
			}
		}
	}
}
