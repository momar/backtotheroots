﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlattform : MonoBehaviour {
	public float speed; // [m/s]
	public List<Transform> positions;
	public int repeat = -1;

	private int goalIdx = 0;
	[HideInInspector] public Vector3 moveVec = Vector3.zero;

	void Start() {
		if (positions == null || positions.Count == 0 || positions[0] == null) {
			return;
		}
		this.gameObject.transform.position = positions[0].position;
		goalIdx++;
	}

    void FixedUpdate() {
		if (positions == null || positions.Count == 0 || positions[0] == null) {
			return;
		}
		if (repeat != 0) {
			moveVec = (positions[goalIdx].position - this.gameObject.transform.position).normalized * speed;
			this.gameObject.transform.position = this.gameObject.transform.position + moveVec;
			float distance = Vector3.Distance(this.gameObject.transform.position, positions[goalIdx].position);
			if (distance < 0.1f && (repeat > 0 || repeat == -1)) {
				goalIdx = (goalIdx + 1) % positions.Count;
				repeat -= (repeat != -1 ? 1 : 0);
			}
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		if (repeat != 0 && other.gameObject.tag == "Player") {
			other.gameObject.transform.Translate(new Vector3(moveVec.x, 0.0f, 0.0f));
		}
	}
}
