﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateStoryEnd : State {
	private int story_counter = 0;
	public StateStoryEnd(GameManager gm) : base(gm) { }

	public override State DoEvent(StateEvent stateEvent) {
		State newState;
		switch (stateEvent) {
			case StateEvent.EndGame:
				this.OnInactive();
				SceneManager.LoadScene(0);
				newState = new StateMenu(gameManager);
				newState.OnActive();
				return newState;
		}
		return this;
	}

	public override void OnActive() {
		gameManager.ui_story_end.SetActive(true);
		gameManager.ui_bubble_end.sprite = gameManager.ui_bubble_left;
		gameManager.ui_bubble_end_text.text = gameManager.story_end_texts[story_counter];
	}

	public override void OnInactive() {
		gameManager.ui_story_end.SetActive(false);
	}

	public override State OnUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			story_counter++;
			if (story_counter == gameManager.story_end_texts.Count) {
				SceneManager.LoadScene(0);
				this.OnInactive();
				State newState = new StateMenu(gameManager);
				newState.OnActive();
				return newState;
			}
			else {
				if ((story_counter % 2) == 0) {
					gameManager.ui_bubble_end.sprite = gameManager.ui_bubble_left;
				}
				else {
					gameManager.ui_bubble_end.sprite = gameManager.ui_bubble_right;
				}
				gameManager.ui_bubble_end_text.text = gameManager.story_end_texts[story_counter];
			}
		}
		return this;
	}
}
