﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StatePaused : State {
	public StatePaused(GameManager gm) : base(gm) {}

	public override State DoEvent(StateEvent stateEvent) {
		State newState;
		switch (stateEvent) {
			case StateEvent.OpenMenu:
				SceneManager.LoadScene(0);
				this.OnInactive();
				newState = new StateMenu(gameManager);
				newState.OnActive();
				return newState;
			case StateEvent.Resume:
				this.OnInactive();
				newState = new StateGame(gameManager);
				newState.OnActive();
				return newState;
		}
		return this;
	}

	public override void OnActive() {
		Time.timeScale = 0.0f;
		gameManager.ui_paused.SetActive(true);
	}

	public override void OnInactive() {
		gameManager.ui_paused.SetActive(false);
		Time.timeScale = 1.0f;
	}

	public override State OnUpdate() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			this.OnInactive();
			State newState = new StateGame(gameManager);
			newState.OnActive();
			return newState;
		}
		return this;
	}
}
