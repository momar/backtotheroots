﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateEvent {OpenMenu, StartGame, EndGame, OpenCredits, Pause, Resume, StartIntro, Retry, Dead, Win};

abstract public class State {
	protected GameManager gameManager;

	public State(GameManager gm) {
		gameManager = gm;
	}

	abstract public State DoEvent(StateEvent stateEvent);
	abstract public void OnActive();
	abstract public void OnInactive();
	abstract public State OnUpdate();
}
