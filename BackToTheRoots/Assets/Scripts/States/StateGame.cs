﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateGame : State {
	public StateGame(GameManager gm) : base(gm) { }

	public override State DoEvent(StateEvent stateEvent) {
		State newState;
		switch (stateEvent) {
			case StateEvent.Dead:
				this.OnInactive();
				newState = new StateLose(gameManager);
				newState.OnActive();
				return newState;
			case StateEvent.Win:
				this.OnInactive();
				newState = new StateStoryEnd(gameManager);
				newState.OnActive();
				return newState;
		}
		return this;
	}

	public override void OnActive() {
		gameManager.ui_game.SetActive(true);
	}

	public override void OnInactive() {
		gameManager.ui_game.SetActive(false);
	}

	public override State OnUpdate() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			this.OnInactive();
			State newState = new StatePaused(gameManager);
			newState.OnActive();
			return newState;
		}
		return this;
	}
}
