﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateIntro : State {
	public StateIntro (GameManager gm) : base(gm) { }

	public override State DoEvent(StateEvent stateEvent) {
		return this;
	}

	public override void OnActive() {
		gameManager.ui_intro.SetActive(true);
	}

	public override void OnInactive() {
		gameManager.ui_intro.SetActive(false);
	}

	public override State OnUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			this.OnInactive();
			//gameManager.level = 0;
			State newState = new StateLoadGame(gameManager);
			newState.OnActive();
			return newState;
		}
		return this;
	}
}
