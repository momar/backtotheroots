﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateStoryBegin : State {
	private int story_counter = 0;
	public StateStoryBegin(GameManager gm) : base(gm) { }

	public override State DoEvent(StateEvent stateEvent) {
		State newState;
		switch (stateEvent) {
			case StateEvent.StartGame:
				this.OnInactive();
				newState = new StateIntro(gameManager);
				newState.OnActive();
				return newState;
		}
		return this;
	}

	public override void OnActive() {
		gameManager.ui_story_begin.SetActive(true);
		gameManager.ui_bubble_begin.sprite = gameManager.ui_bubble_left;
		gameManager.ui_bubble_begin_text.text = gameManager.story_begin_texts[story_counter];
	}

	public override void OnInactive() {
		gameManager.ui_story_begin.SetActive(false);
	}

	public override State OnUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			story_counter++;
			if (story_counter == gameManager.story_begin_texts.Count) {
				this.OnInactive();
				State newState = new StateIntro(gameManager);
				newState.OnActive();
				return newState;
			}
			else {
				if ((story_counter % 2) == 0) {
					gameManager.ui_bubble_begin.sprite = gameManager.ui_bubble_left;
				}
				else {
					gameManager.ui_bubble_begin.sprite = gameManager.ui_bubble_right;
				}
				gameManager.ui_bubble_begin_text.text = gameManager.story_begin_texts[story_counter];
			}
		}
		return this;
	}
}
