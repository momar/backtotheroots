﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateCredits : State {
	public StateCredits(GameManager gm) : base(gm) { }

	public override State DoEvent(StateEvent stateEvent) {
		State newState;
		switch (stateEvent) {
			case StateEvent.OpenMenu:
				this.OnInactive();
				newState = new StateMenu(gameManager);
				newState.OnActive();
				return newState;
		}
		return this;
	}

	public override void OnActive() {
		gameManager.ui_credits.SetActive(true);
	}

	public override void OnInactive() {
		gameManager.ui_credits.SetActive(false);
	}

	public override State OnUpdate() {
		return this;
	}
}
