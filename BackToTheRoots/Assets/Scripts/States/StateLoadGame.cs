﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateLoadGame : State {
	public StateLoadGame (GameManager gm) : base(gm) { }

	public override State DoEvent(StateEvent stateEvent) {
		return this;
	}

	public override void OnActive() {
		/*if (gameManager.levels.Count == 0) {
			for (int i = 0; i < gameManager.level_prefabs.Count; i++) {
				gameManager.levels.Add(GameObject.Instantiate(gameManager.level_prefabs[i], gameManager.level_prefabs[i].transform.position, gameManager.level_prefabs[i].transform.rotation));
			}
		}
		else {
			for (int i = 0; i < gameManager.level_prefabs.Count; i++) {
				Object.Destroy(gameManager.levels[i]);
				gameManager.levels[i] = GameObject.Instantiate(gameManager.level_prefabs[i], gameManager.level_prefabs[i].transform.position, gameManager.level_prefabs[i].transform.rotation);
			}
		}*/

		Transform spawn_lala = GameObject.FindGameObjectWithTag("Spawn" + (gameManager.level + 1) + "Lala").transform;
		Transform spawn_bambam = GameObject.FindGameObjectWithTag("Spawn" + (gameManager.level + 1) + "Bambam").transform;
		Transform spawn_camera = GameObject.FindGameObjectWithTag("Spawn" + (gameManager.level + 1) + "Camera").transform;

		if (!gameManager.playerLala) {
			gameManager.playerLala = GameObject.Instantiate(gameManager.lala_prefab, spawn_lala.position, spawn_lala.rotation);
			gameManager.playerLala.transform.parent = null;
			gameManager.playerLala.name = "Lala";
		}
		else {
			gameManager.playerLala.transform.position = spawn_lala.position;
			gameManager.playerLala.transform.rotation = spawn_lala.rotation;
			gameManager.playerLala.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
		}

		if (!gameManager.playerBambam) {
			gameManager.playerBambam = GameObject.Instantiate(gameManager.bambam_prefab, spawn_bambam.position, spawn_bambam.rotation);
			gameManager.playerBambam.transform.parent = null;
			gameManager.playerBambam.name = "Bambam";
		}
		else {
			gameManager.playerBambam.transform.position = spawn_bambam.position;
			gameManager.playerBambam.transform.rotation = spawn_bambam.rotation;
			gameManager.playerBambam.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

		}

		gameManager.game_camera.transform.position = spawn_camera.position;
		CameraController cc = gameManager.game_camera.GetComponent<CameraController>();
		if (gameManager.level == 1) {
			cc.speed = cc.start_speed;
			cc.autoMode = true;
		}
		else {
			cc.autoMode = false;
		}

		gameManager.musicController.PlayClip(gameManager.level);
	}

	public override void OnInactive() {
		// Do Nothing
	}

	public override State OnUpdate() {
		this.OnInactive();
		State newState = new StateGame(gameManager);
		newState.OnActive();
		return newState;
	}
}
