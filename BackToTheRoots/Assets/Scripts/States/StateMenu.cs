﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMenu : State {
	public StateMenu(GameManager gm) : base(gm) {}

	public override State DoEvent(StateEvent stateEvent) {
		State newState;
		switch (stateEvent) {
			case StateEvent.StartGame:
				this.OnInactive();
				newState = new StateStoryBegin(gameManager);
				newState.OnActive();
				return newState;
			
			case StateEvent.OpenCredits:
				this.OnInactive();
				newState = new StateCredits(gameManager);
				newState.OnActive();
				return newState;

			case StateEvent.EndGame:
				Application.Quit();
				break;
		}
		return this;
	}

	public override void OnActive() {
		gameManager.ui_menu.SetActive(true);
		gameManager.musicController.PlayClip(3);
	}

	public override void OnInactive() {
		gameManager.ui_menu.SetActive(false);
	}

	public override State OnUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			this.OnInactive();
			//gameManager.level = 0;
			State newState = new StateStoryBegin(gameManager);
			newState.OnActive();
			return newState;
		}
		return this;
	}
}
