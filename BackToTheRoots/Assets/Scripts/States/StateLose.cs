﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateLose : State {
	public StateLose(GameManager gm) : base(gm) { }
	public override State DoEvent(StateEvent stateEvent) {
		State newState;
		switch (stateEvent) {
			case StateEvent.OpenMenu:
				this.OnInactive();
				newState = new StateMenu(gameManager);
				newState.OnActive();
				SceneManager.LoadScene(0);
				return newState;
			case StateEvent.Retry:
				this.OnInactive();
				newState = new StateLoadGame(gameManager);
				newState.OnActive();
				return newState;
		}
		return this;
	}

	public override void OnActive() {
		gameManager.game_camera.GetComponent<CameraController>().enabled = false;
		gameManager.ui_lose.SetActive(true);
		gameManager.musicController.PlayClip(4);
	}

	public override void OnInactive() {
		gameManager.game_camera.GetComponent<CameraController>().enabled = true;
		gameManager.ui_lose.SetActive(false);
		//Time.timeScale = 1.0f;
	}

	public override State OnUpdate() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			this.OnInactive();
			State newState = new StateLoadGame(gameManager);
			newState.OnActive();
			return newState;
		}
		return this;
	}
}
