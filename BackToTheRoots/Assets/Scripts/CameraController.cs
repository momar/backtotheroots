﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public GameManager gameManager;
	public List<float> sizes;
	public float start_speed;
	[HideInInspector] public float speed;
	public float offsetY;
	public bool autoMode;
	private Camera cam;

	void Start () {
		cam = this.gameObject.GetComponent<Camera>();
		speed = start_speed;
	}

	void FixedUpdate() {
		cam.orthographicSize = sizes[gameManager.level];
		if (autoMode) {
			
			this.gameObject.transform.position = new Vector3 (this.gameObject.transform.position.x, this.gameObject.transform.position.y + speed, this.gameObject.transform.position.z);
		}
		else if (gameManager.playerLala && gameManager.playerBambam) {
			float posY = (gameManager.playerLala.transform.position.y + gameManager.playerBambam.transform.position.y) / 2.0f;
			this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, posY + offsetY, this.gameObject.transform.position.z);
		}
	}
}
