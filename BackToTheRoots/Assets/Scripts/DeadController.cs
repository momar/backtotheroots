﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadController : MonoBehaviour {
	[HideInInspector] public GameManager gameManager;

	void Start() {
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "Player") {
			Debug.Log(this.gameObject.name);
			Debug.Log(other.gameObject.name);
			other.attachedRigidbody.constraints = 0;
			other.attachedRigidbody.velocity += new Vector2(50.0f, 50.0f);
			gameManager.OnStateEvent("Dead");
		}
	}
}
