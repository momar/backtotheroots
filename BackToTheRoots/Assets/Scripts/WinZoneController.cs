﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinZoneController : MonoBehaviour {
    [HideInInspector] public GameManager gameManager;

	void Start () {
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player") {
			gameManager.OnStateEvent("Win");
		}
	}
}
