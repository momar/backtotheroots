﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	[HideInInspector] public List<GameObject> level_prefabs;
	public GameObject ui_menu;
	public GameObject ui_credits;
	public GameObject ui_intro;
	public GameObject ui_game;
	public GameObject ui_paused;
	public GameObject ui_lose;
	public GameObject ui_story_begin;
	public GameObject ui_story_end;
	public Image ui_bubble_begin;
	public Image ui_bubble_end;
	public Text ui_bubble_begin_text;
	public Text ui_bubble_end_text;
	public Sprite ui_bubble_left;
	public Sprite ui_bubble_right;
	public List<string> story_begin_texts;
	public List<string> story_end_texts;

	public GameObject lala_prefab;
	public GameObject bambam_prefab;
	public GameObject game_camera;
	public MusicController musicController;
	public AudioClip se_lose;

	[HideInInspector] public bool paused;
	[HideInInspector] public GameObject playerLala;
	[HideInInspector] public GameObject playerBambam;

	public List<GameObject> levels;
	public int level = 0;
	[HideInInspector] public State state;

	void Start () {
		state = new StateMenu(this);
		state.OnActive();
		levels.Clear();
	}

	public void OnStateEvent(string stateEvent) {
		StateEvent se = (StateEvent)Enum.Parse(typeof(StateEvent), stateEvent, true);
		state = state.DoEvent(se);
	}

	void Update () {
		state = state.OnUpdate();
	}
}
