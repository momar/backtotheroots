﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelController : MonoBehaviour {
	public int nextLevel;
	public GameManager gameManager;
	[HideInInspector] public AudioSource audioSource;

	void Start () {
		audioSource = this.gameObject.GetComponent<AudioSource>();
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (gameManager.level < nextLevel && other.gameObject.tag == "Player") {
			if(!gameManager) {
				gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
			}
			gameManager.level = nextLevel;
			if (nextLevel == 1) {
				gameManager.game_camera.GetComponent<CameraController>().autoMode = true;
			}
			else {
				gameManager.game_camera.GetComponent<CameraController>().autoMode = false;
			}

			gameManager.musicController.PlayClip(nextLevel);
			audioSource.Play();
		}
	}
}
