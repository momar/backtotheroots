﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpButtonController : MonoBehaviour {
	public AudioSource audioSource;
	public List<Interactable> interactables;
	public Sprite spriteWhenOn;
	public Sprite spriteWhenOff;
	public bool isTaster;
	public bool isToggle;

	private bool isActive;

	void Start () {
		audioSource = this.gameObject.GetComponent<AudioSource>();
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (!isActive && (other.gameObject.tag == "Player" || other.gameObject.tag == "Trigger")) {
			audioSource.Play();
			foreach (var interactable in interactables) {
				if (isToggle) {
					interactable.ExecuteToggle();
				}
				else {
					interactable.ExecuteEnter();
				}
			}
			this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteWhenOn;
			if (!isTaster) {
				isActive = true;
			}
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (isTaster) {
			foreach (var interactable in interactables) {
				interactable.ExecuteExit();
			}
			this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteWhenOff;
		}
		else if (isToggle) {
			this.gameObject.GetComponent<SpriteRenderer>().sprite = spriteWhenOff;
			isActive = false;
		}
	}
}
