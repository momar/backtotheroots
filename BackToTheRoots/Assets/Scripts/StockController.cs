﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockController : MonoBehaviour {
	private AudioSource audioSource;
    public float multiplier = 1f;
    private bool inverted = false;
    private int pushState = -1;
    private Vector2 start, target;

    // Start is called before the first frame update
    void Start()
    {
        if (this.gameObject.transform.localScale.x < 0) {
            inverted = true;
        }
		audioSource = this.gameObject.GetComponent<AudioSource>();

	}

    // Update is called once per frame
    void FixedUpdate() {
        if (pushState > 400) {
            pushState = -1;
        } else if (pushState >= 300) {
            this.gameObject.transform.position = Vector2.Lerp(target, start, (pushState - 300) / 100f);
            pushState += 5;
        } else if (pushState > 100) {
            pushState++;
        } else if (pushState >= 0) {
            this.gameObject.transform.position = Vector2.Lerp(start, target, pushState / 100f);
            pushState += 10;
        }

		if (!audioSource.isPlaying && (pushState >= 0 && pushState < 300) || (pushState > 400)) {
			audioSource.Play();
		}
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (pushState > -1) {
            return;
        }
        start = this.gameObject.transform.position;
        target = start + new Vector2(0.5f * (inverted ? -1 : 1), 0.25f) * multiplier;
		pushState = 0;
	}
}
