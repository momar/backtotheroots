﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	private GameManager gameManager;
	private Rigidbody2D rb;

	private AudioSource audioSource;
	public AudioClip audio_jump;
	public List<AudioClip> audio_moves;

	public Animator anim;
	public Transform groundSensorLeft;
	public Transform groundSensorRight;
	public Transform sensorLeft;
	public Transform sensorRight;

	public KeyCode moveLeft;
	public KeyCode moveRight;
	public KeyCode moveJump;

	public float speed = 5.0f;
	public float jumpForce = 20.0f;
	public float downForceP = 0.3f;
	public float moveInAirInPercent = 0.05f;

	private bool doMoveLeft;
	private bool doMoveRight;
	private bool doMoveJump;
	private bool doMoveJumpEnd;

	private bool isJumping;
	private bool isExhausted;
	private bool jumped;
	private bool moved;

    void Start() {
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		audioSource = this.gameObject.GetComponent<AudioSource>();
		rb = this.gameObject.GetComponent<Rigidbody2D>();
	}

    void Update() {
		if (!gameManager.paused) {
			InputController();
		}
	}

	void FixedUpdate() {
		if (!gameManager.paused) {
			LogicController();
		}
	}

	private void InputController() {
		doMoveLeft = Input.GetKey(moveLeft);
		doMoveRight = Input.GetKey(moveRight);
		doMoveJump = Input.GetKey(moveJump);
		doMoveJumpEnd = Input.GetKeyUp(moveJump);
	}

	private void LogicController() {
		float distanceGroundLeft = Physics2D.Raycast(groundSensorLeft.position, Vector2.down, 100.0f, 1 << LayerMask.NameToLayer("Grid") | 1 << LayerMask.NameToLayer("Trigger")).distance;
		float distanceGroundRight = Physics2D.Raycast(groundSensorRight.position, Vector2.down, 100.0f, 1 << LayerMask.NameToLayer("Grid") | 1 << LayerMask.NameToLayer("Trigger")).distance;

		float distanceLeft = Physics2D.Raycast(sensorLeft.position, Vector2.left, 100.0f, 1 << LayerMask.NameToLayer("Grid") | 1 << LayerMask.NameToLayer("Trigger")).distance;
		float distanceRight = Physics2D.Raycast(sensorRight.position, Vector2.right, 100.0f, 1 << LayerMask.NameToLayer("Grid") | 1 << LayerMask.NameToLayer("Trigger")).distance;

		isJumping = distanceGroundLeft > 0.01f && distanceGroundRight > 0.01f;

		Vector2 jumpVec = Vector2.zero;
		Vector3 moveVec = Vector3.zero;

		if (doMoveLeft && (distanceLeft > 0.1f || distanceLeft == 0.0f)) {
			anim.SetBool("Move_Left", true);
			moveVec += new Vector3(-speed, 0.0f, 0.0f) * (isJumping ? moveInAirInPercent : 1.0f);
		}
		else {
			anim.SetBool("Move_Left", false);
			audioSource.Pause();
		}

		if (doMoveRight && (distanceRight > 0.1f || distanceRight == 0.0f)) {
			anim.SetBool("Move_Right", true);
			moveVec += new Vector3(speed, 0.0f, 0.0f) * (isJumping ? moveInAirInPercent : 1.0f);
		}
		else {
			anim.SetBool("Move_Right", false);
		}

		if (doMoveJumpEnd) {
			isExhausted = true;
			jumpVec -= new Vector2(0.0f, downForceP);
		}

		if (!isJumping) {
			isExhausted = false;
		}

		if (doMoveJump && !isJumping && !isExhausted) {
			if (!isJumping) {
				audioSource.clip = audio_jump;
				jumpVec += new Vector2(0.0f, jumpForce);
				isExhausted = true;
				jumped = true;
			}
		}

		rb.velocity += jumpVec;
		this.gameObject.transform.Translate(moveVec);

		if (!audioSource.isPlaying) {
			if (jumped) {
				audioSource.clip = audio_jump;
				audioSource.Play();
				jumped = false;
			}
			else if (moved) {
				int idx = Random.Range(0, audio_moves.Count);
				audioSource.clip = audio_moves[idx];
				audioSource.Play();
				moved = false;
			}
		}
	}
}
